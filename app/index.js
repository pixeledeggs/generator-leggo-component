'use strict';

const Generator = require('yeoman-generator');

module.exports = class extends Generator {
	constructor(args, opts) {
		super(args, opts);
		this.log('...Initializing v1.0.5...');
	}
	start() {
		
		this.log('Start...');

		this.prompt([
		{
			type    : 'input',
			name    : 'name',
			message : 'Enter a name for the new component (i.e.: myNewComponent): '
		},
		{
			type    : 'input',
			name    : 'path',
			message : 'Enter the destination path (leaving blank defaults to ../library/component/): '
		}
		]).then( (answers) => {
			
			const fs = require('fs');

			let path = (answers.path != '') ? answers.path : '../library/component/';
			let name = (answers.name != '') ? answers.name : '';
			
			let function_name = answers.name;
			function_name = function_name.replace(/-/g, '_');
			
			path = path + name;

			this.log('Path: ' + path);
			
			if (fs.existsSync(path)) {	
				this.log('The path ' + path + ' already exists, nothing happend (◕︿◕✿)');
				return;
			}

			this.log('Comnponent written to path ' + path + ' ヽ(•‿•)ノ');

			// create destination folder
			this.destinationRoot(path);
			
			// copy component files to destination folder
			this.fs.copyTpl(
				this.templatePath('functions.php'),
				this.destinationPath(answers.name + '-functions' + '.php'),
				{ theme_init_component: answers.name, theme_init_component_function: function_name },
			);
			
			this.fs.copyTpl(
				this.templatePath('index.php'),
				this.destinationPath(answers.name + '.php'),
				{ theme_init_component: answers.name }
			);
			
			this.fs.copyTpl(
				this.templatePath('script.js'),
				this.destinationPath(answers.name + '.js'),
				{ theme_init_component: answers.name }
			);
			
			this.fs.copyTpl(
				this.templatePath('style.scss'),
				this.destinationPath(answers.name + '.scss'),
				{ theme_init_component: answers.name }
			);

			this.fs.copyTpl(
				this.templatePath('acf-export.json'),
				this.destinationPath(answers.name + '-acf-export.json'),
				{ theme_init_component: answers.name }
			);
			
			this.fs.copyTpl(
				this.templatePath('md.md'),
				this.destinationPath(answers.name + '.md'),
				{ theme_init_component: answers.name }
			);

		});
	}
};
