<?php 
defined('ABSPATH') or die("");

/**
 * Functions for component <%= theme_init_component %>
 * Include by include_components() 
 * Or manually add 
 * include_once ($template_dir."/library/component/<%= theme_init_component_function %>/<%= theme_init_component_function %>-functions.php");
 */

/**
 * theme_has_component_<%= theme_init_component %>
 * Used to check if component function theme_has_component_<%= theme_init_component_function %> exists
 */
function theme_has_component_<%= theme_init_component_function %>(){ return true; }

if( !defined('LEGGO_ACF_GUI') ):
    
    /**
     * ACF Field Group for <%= theme_init_component %>
     */
    
endif;
