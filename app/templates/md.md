# Leggo Piece

<%= theme_init_component %>

___

## Flexible Content Integration

Clone to flexible content field group  
Using the following settings as a new layout

Label : TBA

Name : TBA

Layout: Block (default)

___

## Fields

Field Label : TBA

Field Name : TBA

Field Type : TBA

Fields : All fields from `TBA`

Display : Seamless / Group 

___

## Notes 

Check if the .js is required to be imported into the projects exportfiles.json
Check if the sass file is required to be imported into the projects app.scss
