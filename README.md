# README v1.0.5

Yeoman generator for creating WP self contained components 

## Self contained component 

A component is a self-contained building-block for containing it's own fields, PHP template, PHP functions, scripts, and styles.

ExampleComponent/\

  ├── ExampleComponent-acf-export.json\
  ├── ExampleComponentFunctions.php\
  ├── ExampleComponent.js\
  ├── ExampleComponent.md\
  ├── ExampleComponent.php\
  ├── ExampleComponent.scss

## Installation

`$ npm install -g generator-leggo-component`

`$ yo leggo-component`\
    `Initializing...`\
    `? Enter a name for the new component (i.e.: myNewComponent):  myNewComponent`

## Created using this Yeoman guide 

https://medium.com/@vallejos/yeoman-guide-adea4d6ea1e3

## NPM Source 
https://www.npmjs.com/package/generator-leggo-component

## Bitbucket Repository
https://bitbucket.org/pixeledeggs/generator-leggo-component/src/master/
